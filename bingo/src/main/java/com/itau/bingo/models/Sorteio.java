package com.itau.bingo.models;

import java.util.HashSet;

public class Sorteio {
	
	private int numero;
	private HashSet<Integer> numeros;
	private boolean status;
	
	public Sorteio() {
		setNumeros(new HashSet<Integer>());
		setStatus(false);
	}

	public void gerarNumeroSorteio() {
		
		while (numeros.size() < 32) {
			int valorSorteio  = (int) Math.ceil(Math.random() * 32 - 1);
			setNumero(valorSorteio);
			numeros.add(valorSorteio);
		}

	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public HashSet<Integer> getNumeros() {
		return numeros;
	}

	public void setNumeros(HashSet<Integer> numeros) {
		this.numeros = numeros;
	}
	
	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
}
