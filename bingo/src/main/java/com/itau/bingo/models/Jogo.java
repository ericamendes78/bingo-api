package com.itau.bingo.models;

import java.util.ArrayList;
import java.util.List;

public class Jogo {
	
	private boolean statusJogo;
	public static ArrayList<Integer> numerosSorteado;
	
	public Jogo() {
		numerosSorteado = new ArrayList<Integer>();
	}


	public boolean isStatusJogo() {
		return statusJogo;
	}

	public void setStatusJogo(boolean statusJogo) {
		this.statusJogo = statusJogo;
	}
	
	
	public boolean existeNumeroCartela(int valor){
		boolean existeNumero = false;
		
		Sorteio s = new Sorteio();
		numerosSorteado.add(s.getNumero());
		
		Cartela c = new Cartela("");
		
		
		List<Integer> numeros = new ArrayList<Integer>(c.gerarNumeroCartela());

		
		if(numeros.contains(numerosSorteado)) {
			existeNumero = true;
			numeros.remove(valor);
		}
		
		return existeNumero;
	}

}
