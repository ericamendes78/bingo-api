package com.itau.bingo.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class Cartela {

	private static int id;
	private String titulo;
	private static int[][] cartela;
	private int quantidadeAcertos;

	public Cartela(String titulo) {
		setId(getId() + 1);
		setTitulo(titulo);
		setCartela(new int[4][4]);
	}

	public String getTitulo() {
		return titulo;
	}

	private void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int[][] getCartela() {
		return cartela;
	}

	private static void setCartela(int[][] cartela) {
		Cartela.cartela = cartela;
	}
	
	public int getId() {		
		return id;
	}

	private void setId(int id) {
		Cartela.id = id;
	}

	public int getQuantidadeAcertos() {
		return quantidadeAcertos;
	}

	public void setQuantidadeAcertos(int quantidadeAcertos) {
		this.quantidadeAcertos = quantidadeAcertos;
	}

	public HashSet<Integer> gerarNumeroCartela() {

		HashSet<Integer> numeros = new HashSet<Integer>();

		while (numeros.size() < 16) {
			int valor = (int) Math.ceil(Math.random() * 60 - 1);
			numeros.add(valor);
		}

		return numeros;

	}

	public void preencherCartela() {

		List<Integer> numeros = new ArrayList<Integer>(gerarNumeroCartela());
		
		Collections.sort(numeros);

		for (int i = 0; i < cartela.length; i++) {
			for (int j = 0; j < cartela.length; j++) {
				cartela[i][j] = numeros.get(j + i * 4);
			}
		}

		setCartela(cartela);

	}

	public void pesquisarItemCartela(int valor) {

		for (int i = 0; i < cartela.length; i++) {
			for (int j = 0; j < cartela.length; j++) {
				if(cartela[i][j] == valor) {
					quantidadeAcertos += 1;
				}
			}
		}

	}	
	
}
