package com.itau.bingo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.itau.bingo.models.Cartela;
import com.itau.bingo.models.Jogo;
import com.itau.bingo.models.Sorteio;

/**
 * Hello world!
 *
 */

@SpringBootApplication
public class App {
	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}
}
