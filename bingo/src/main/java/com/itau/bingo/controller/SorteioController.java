package com.itau.bingo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.bingo.models.Cartela;
import com.itau.bingo.models.Sorteio;

@RestController
public class SorteioController {
	
	@RequestMapping(method=RequestMethod.POST,path="/sorteio/validar")
	public ResponseEntity<?> postValidarSorteio(@RequestBody Sorteio sorteio, Cartela cartela) {

		for(Integer s : sorteio.getNumeros()) {
			cartela.pesquisarItemCartela(s);
		}
		
		cartela.getQuantidadeAcertos();
		
		sorteio.setNumeros(null);
		sorteio.setNumero(0);
		sorteio.setStatus(false);
				
		return ResponseEntity.status(400).body(cartela);
	}
	
	@RequestMapping("/sorteio")
	public ResponseEntity<?> getSorteio(){
		Sorteio s = new Sorteio();
		s.gerarNumeroSorteio();

		return ResponseEntity.ok(s);
	}
	
	@RequestMapping(method=RequestMethod.POST,path="/sorteio/reiniciar")
	public ResponseEntity<?> postReiniciarSorteio(@RequestBody Sorteio sorteio) {
		sorteio.setNumeros(null);
		sorteio.setNumero(0);
		sorteio.setStatus(false);
		return ResponseEntity.status(400).body(sorteio);
	}
	
	
}
