package com.itau.bingo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itau.bingo.models.Cartela;

@RestController
public class CartelaController {
		
	@RequestMapping("/cartela")
	public ResponseEntity<?> getCartela(){
		Cartela c = new Cartela("Bingo do Bruno");	
		c.preencherCartela();
		return ResponseEntity.ok(c);
	}
	
}
